import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule} from '@angular/forms';
import{HttpModule} from '@angular/http';
import{routing,appRoutingProviders} from './app.routing';
import { AppComponent } from './app.component';
import{UserEditComponent} from './components/user-edit.component';
import{AgregarPreguntaComponent} from './components/agregar-pregunta.component';
import{ConfigurarTestComponent} from './components/configurar-test.component';
import{EditarPreguntaComponent} from './components/editar-pregunta.component';
import{MisTestOficialComponent} from './components/mis-test-oficial.component';
import{PanelControlAlumnosComponent} from './components/panel-control-alumnos.component';
import{AdministracionUsuariosComponent} from './components/administracion-usuarios.component';
import{JuegosCanvasComponent} from './components/juegos.component';
import{Ingsistemascomponent} from './components/ingenieriasistemas.component';




@NgModule({
  declarations: [
    AppComponent,
    UserEditComponent,
    AgregarPreguntaComponent,
    ConfigurarTestComponent,
    EditarPreguntaComponent,
    MisTestOficialComponent,
    PanelControlAlumnosComponent,
    AdministracionUsuariosComponent,
    JuegosCanvasComponent,
    Ingsistemascomponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [appRoutingProviders,{ provide: 'Window',  useValue: window }],
  bootstrap: [AppComponent]
})
export class AppModule {}
