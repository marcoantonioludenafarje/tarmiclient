import{TestResultado,PuntajeSkills} from '../models/testResultado';

export class User{
    public cuentanosDeTi:Object;
    // public puntajeSkills:PuntajeSkills[];
    public testResultado:TestResultado;
    public colegio:string;
    public gradoEstudio:string;
    public codigoColegio:string;
    public sexo;
    public atc:string;
    public rPassword:string;
    public ruc:string;
    public claveAccesoAlumno:string;
    public permisoAterceros:{
        colegio:string;
        profesores:[string];
    };
    public credenciales:{
        colegioClave:string;
        mentorClave:[string]
    };
    public parametros:{
        verficarCodAcceso:Boolean;
    };
    constructor(
        public _id:string,
        public name:string,
        public surname:string,
        public email:string,
        public password:string,
        public role:string,
        public image:string,
    ){}

}