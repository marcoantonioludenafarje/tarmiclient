export class Song{
    constructor(
        public _number:string,
        public _name:string,
        public _duration:string,
        public _file:string,
        public _album:string

    ){}

}