export class UserFinal{
    constructor(
        public _id:string,
        public codigo:string,
        public password:string,
        public nombres:string,
        public apellidos:string,
        public correo:string,
        public telefono:string,
        public dni:string,
        public direccion:{
            pais:String,
            provincia:String,
            distrito:String,
            direcccionExacta:String
        },
        public fechaNacimiento:Date,
        public fechaCreacionCuenta:Date,
        public colegio:String
    ){}

}