import {ModuleWithProviders} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import {UserEditComponent} from './components/user-edit.component';
import{AgregarPreguntaComponent} from './components/agregar-pregunta.component';
import{ConfigurarTestComponent} from './components/configurar-test.component';
import{EditarPreguntaComponent} from './components/editar-pregunta.component';
import{MisTestOficialComponent} from './components/mis-test-oficial.component';
import{PanelControlAlumnosComponent} from './components/panel-control-alumnos.component';
import{AdministracionUsuariosComponent} from './components/administracion-usuarios.component';
import{JuegosCanvasComponent} from './components/juegos.component';
import{Ingsistemascomponent} from './components/ingenieriasistemas.component';

const appRoutes:Routes=[
    {path:'mis-datos',component:UserEditComponent},
    {path:'configurar-test/:page',component:ConfigurarTestComponent},
    {path:'crear-pregunta',component:AgregarPreguntaComponent},
    {path:'editar-pregunta/:id',component:EditarPreguntaComponent},
    {path:'mis-test-oficial',component:MisTestOficialComponent},
    {path:'analitycs',component:PanelControlAlumnosComponent},
    {path:'administracion-usuarios',component:AdministracionUsuariosComponent},
    {path:'juegos-canvas',component:JuegosCanvasComponent},
    {path:'ingsis',component:Ingsistemascomponent},
    {path:'**',component:MisTestOficialComponent}

];


export const appRoutingProviders:any[]=[];
export const routing:ModuleWithProviders=RouterModule.forRoot(appRoutes);





