import { Component,OnInit } from '@angular/core';
import {UserService} from './services/user.service';
import { User } from './models/user';
import {GLOBAL} from './services/global';
import { UserFinal } from './models/userfinal';
import {Router,ActivatedRoute,Params} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers:[UserService]
})
export class AppComponent implements OnInit {
  public title='INGRESAR COMO';
  public user:User;
  public user_register:User;
  public userFinal;
  public identity;
  public token:String;
  public errorMessage;
  public errorMessageRegister;
  public errorMessageRegisterFacebook;
  public url:string;
  public authenticacion;
  public datosBasicos;
  public mostrarRegistro=false;
  public tipoRegistro;
  //Estos son lo de mi formulario
  public mostrarTodo;
  public elementos:any;
  public opciones:any;
  public terminos:any;
  public ocultar=false;
  public mostrarAyudante=false;
  public errorCodActivacion;
  public codigoGenerado="";
  public mostrarCodigo=false;
      constructor(
        private _userService:UserService,
        private _route:ActivatedRoute,
        private _router:Router

      ){
        this.user =new User('','','','','','','');
        this.user_register=new User('','','','','','','');
        this.user_register.sexo="";
                this.url=GLOBAL.url;
        
      }
      
  ngOnInit(){
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.mostrarTodo=false;
    this.user_register.gradoEstudio="Año de estudio"

}

  public onSubmit(){
    console.log("Holi Boli");
    console.log(this.user.email);
    console.log(this.user.password);

    this.elementos=document.getElementById("formulario-registro").getElementsByClassName("input-group");
  if(!this.validarInputLoginNormal()){
      console.log("Falta validar los inputs")
    }else{
      console.log("Envia correctamente papu");

    this._userService.signup(this.user).subscribe(
      response=>{
        let identity=response.user;
        this.identity=identity;
        if(!this.identity._id){
          alert("El usuario no esta correctamente identificado");
        }else{
          //Crear elemento en el localstorage para tener al usuario sesion
          localStorage.setItem('identity',JSON.stringify(identity));
          //Conseguir el token para enviarselo a cada peticion http}
//ACA EMPIEZA
                this._userService.signup(this.user,'true').subscribe(
                    response=>{
                      let token=response.token;
                      this.token=token;
                      console.log(token);
                      if(!this.token){
                        alert("El tokem no se ha generado");
                      }else{
                        // Crear un elemento en el localstorage para tener un token disponible 
                        localStorage.setItem('token',token);
                        console.log("El token se ha generado y eres el papu d elos papus");
                        console.log(token);
                        console.log(identity);
                        this._router.navigate(['/mis-test-oficial']);
                      }

                    },error=>{
                      var errorMessage=<any>error;
                      if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        this.errorMessage=body.message;
                        console.log(error);
                      }
                    }
                  );



//ACA TERMINA

        }

      },error=>{
        var errorMessage=<any>error;
        if(errorMessage!=null){
          var body=JSON.parse(error._body);
          this.errorMessage=body.message;
          console.log(error);
        }
      }
    );
    }
  }
    validarInputLoginNormal(){
    for(var i=0;i<this.elementos.length;i++){
      if(this.elementos[i].children[0].type=="email" || this.elementos[i].children[0].type=="password"){
        if(this.elementos[i].children[0].value==""){
          console.log("El campo "+ this.elementos[i].children[0].name+ " esta completo");
          this.elementos[i].children[0].className=this.elementos[i].children[0].className + ' error';
          return false;
        }else{
          this.elementos[i].children[0].className=this.elementos[i].children[0].className.replace("error", "");
        }

      }

    }
    // if(this.elementos[2].children[0].value!==this.elementos[3].children[0].value){
    //   this.elementos[2].children[0].value="";
    //   this.elementos[3].children[0].value="";
    //   this.elementos[2].children[0].className=this.elementos[2].children[0].className + ' error';
    //   this.elementos[3].children[0].className=this.elementos[3].children[0].className + ' error';
    // }else{
    //   this.elementos[2].children[0].className=this.elementos[2].children[0].className.replace("error", "");
    //   this.elementos[3].children[0].className=this.elementos[3].children[0].className.replace("error", "");
    
    // }
    return true;
  }




    // onButtonClick() {
    //     this.titleKendo = 'Hello from Kendo UI!';
    // }

    logout(){
      localStorage.removeItem('identity');
      localStorage.removeItem('token');
      localStorage.clear();
      this._router.navigate(['/']);
      this.identity=null;
      this.token=null; 
      this.user=new User('','','','','','','');

    }

    onSubmitRegister(tipoUsuario){
      this.elementos=document.getElementById("UserregisterForm").getElementsByClassName("input-group");
      this.errorMessageRegister="Falta completar los siguientes campos :";  
      if(!this.validarInputs()){
          console.log("Falta validar kis inputs")
        }else if(!this.validarRadios()){
          console.log("Falta validar los radio")
        }else if(!this.validarCheckbox()){
          console.log("Falta validar los checkbox")    
        }else{
          // console.log("Envia correctamente");
          // console.log("Veamos el usuario de registro");
          console.log(this.user_register);
          this._userService.register(this.user_register,tipoUsuario).subscribe(
          response=>{
            let user=response.user;
          //   this.user_register=user;
            if(!user._id){
              alert("Error al registrarse");
              this.errorMessageRegister="Error al registrarse";
            }else{
              this.errorMessageRegister="Registro se ha realizado correctamente, procede a logearte con"+ this.user_register.email;
              //Con esto dejo en blanco el registro
              this.user_register=new User('','','','','','','');
              this.errorMessageRegister="Se realizo de forma exitosa el registro.Por favor proceda a loguearse";
              this.mostrarRegistro=false;
          }
          console.log("Aca viene la respuesta");
          console.log(response); 
          },
          error=>{
              var errorMessageRegister=<any>error;
            if(errorMessageRegister!=null){
              var body=JSON.parse(error._body);
              this.errorMessageRegister=body.message;
            }
          }


        );
        }
    // this._userService.register(this.user_register).subscribe(
    //   response=>{
    //     let user=response.user;
    //     this.user_register=user;
    //     if(!user._id){
    //       alert("Error al registrarse");
    //       this.errorMessageRegister="Error al registrarse";
    //     }else{
    //       console.log("Entro bien a esta parte");
    //       this.errorMessageRegister="Registro se ha realizado correctamente, procede a logearte con"+ this.user_register.email;
    //       this.user_register=new User('','','','','','ROLE_USER','');
    //   }
    //   },
    //   error=>{
    //       var errorMessageRegister=<any>error;
    //     if(errorMessageRegister!=null){
    //       var body=JSON.parse(error._body);
    //       this.errorMessageRegister=body.message;
    //     }
    //   }


    // );

  }



  //ACA VA LO DE LOS FORMULARIOS
  // public formulario=document;
  // public elementos=formulario.elements;

 
  enviar(){
this.elementos=document.getElementById("formulario-registro").getElementsByClassName("input-group");
  console.log(this.elementos);  
  if(!(this.validarInputs() && this.validarRadios() && this.validarCheckbox())){
      console.log("Falta algo");
    }else{
      console.log("Envia correctamente");
    }

  }


  validarInputs(){
    var verdad=true;
    for(var i=0;i<this.elementos.length;i++){
      console.log("Atentos con los imputs");
      console.log(this.elementos[i].children[0].type);
      if(this.elementos[i].children[0].type=="text" || this.elementos[i].children[0].type=="email" || this.elementos[i].children[0].type=="password" || this.elementos[i].children[0].type=="select-one"){
        if(i!=5){
          if(this.elementos[i].children[0].value=="" || this.elementos[i].children[0].value=="Año de estudio"){
            console.log("El campo "+ this.elementos[i].children[0].name+ " esta completo");
            this.errorMessageRegister=this.errorMessageRegister+"- " +this.elementos[i].children[0].name+" -";
            this.elementos[i].children[0].className=this.elementos[i].children[0].className + ' error';
           
          }else{
            this.elementos[i].children[0].className=this.elementos[i].children[0].className.replace("error", "");
          }
        }

      }

    }
    if(this.elementos[2].children[0].value!==this.elementos[3].children[0].value){
      this.elementos[2].children[0].value="";
      this.elementos[3].children[0].value="";
      this.elementos[2].children[0].className=this.elementos[2].children[0].className + ' error';
      this.elementos[3].children[0].className=this.elementos[3].children[0].className + ' error';
      this.errorMessageRegister=this.errorMessageRegister+"-  Las contraseñas no son iguales -";
      return false;
    }else{
      this.elementos[2].children[0].className=this.elementos[2].children[0].className.replace("error", "");
      this.elementos[3].children[0].className=this.elementos[3].children[0].className.replace("error", "");
    
    }
    return true;
  }


  validarRadios(){
    this.opciones=document.getElementsByName('sexo');
    var resultado=false;
    
    for(var i=0;i<this.opciones.length;i++){
      if(this.opciones[i].checked){
        resultado=true;
        break;
      }
    }
    if(resultado==false){
      this.errorMessageRegister=this.errorMessageRegister+ "-genero-";
      this.opciones[0].parentElement.className=this.opciones[0].parentElement.className+ ' error';
      return false;
    }else{
      this.opciones[i].parentElement.className=this.opciones[i].parentElement.className.replace(" error","");
      return true;
    }
  }
  validarCheckbox(){
    this.terminos=document.getElementsByName('atc');
    var resultado=false;
    
    for(var i=0;i<this.terminos.length;i++){
      if(this.terminos[i].checked){
        resultado=true;
        break;
      }
    }
    if(resultado==false){
      this.terminos[0].parentElement.className=this.terminos[0].parentElement.className+ ' error';
         this.errorMessageRegister=this.errorMessageRegister+ "-terminos y condiciones -";
      console.log("Los terminos y condiciones no han sido aceptados");
      return false;
    }else{
      this.terminos[i].parentElement.className=this.terminos[i].parentElement.className.replace(" error","");
      return true;
    }
  }
  focusInput(event){
    console.log(event);
    event.srcElement.parentElement.children[1].className="label active";
    console.log("aca viene algo bien chido");
    console.log(event.srcElement.parentElement.children[0].className);
    event.srcElement.parentElement.children[0].className=event.srcElement.parentElement.children[0].className.replace("error","")

  }
  blurInput(event){
    //No poner en caso de no ser obligatorio
    if(event.srcElement.value<=0){
    event.srcElement.parentElement.children[1].className="label";
    event.srcElement.parentElement.children[0].className=event.srcElement.parentElement.children[0].className + " error";
    }

  }
  blurInputNormal(event){
      if(event.srcElement.value<=0){
    event.srcElement.parentElement.children[1].className="label";
    // event.srcElement.parentElement.children[0].className=event.srcElement.parentElement.children[0].className + " error";
    }  
  }
  siguiente(){
    document.getElementById("tata").className="contenedor_tarjeta movimientoVerticalArriba";
    document.getElementById("wawa").className="contenedor_tarjeta movimientoVerticalArriba";
  }
  atras(){
    document.getElementById("tata").classList.remove("movimientoVerticalArriba");
    document.getElementById("wawa").classList.remove("movimientoVerticalArriba");
    
  }
  loguearte(){
    document.getElementById("caraWawa").className="mueveteHorizontal";
    document.getElementById("wawaLogeate").className="mueveteHorizontal";
  }
  loguearteFacebook(){
    console.log("aca viene el logueo con facebook");
  }
  cambiarSideBar(){
    if(this.ocultar){
    document.getElementById("sidebar").classList.remove("mostrarHorizontal");
    document.getElementById("bars").classList.remove("voltearBar");
    
    this.ocultar=false;
    }else{
    console.log("Holi BOli");
    document.getElementById("sidebar").className=document.getElementById("sidebar").className + " mostrarHorizontal";
    document.getElementById("bars").className=document.getElementById("bars").className+" voltearBar";
    this.ocultar=true;
    }
   
  }
  registrarte(num){
    this.tipoRegistro=num;
    this.mostrarRegistro=true;
    if(num==0){
    this.title="REGISTRO ESTUDIANTE";
    }else if(num==1) {
    this.title="REGISTRO MENTOR";
    }else if(num==2){
      this.title="REGISTRO COLEGIO";
    }
  }
  regresarLogin(){
    this.mostrarRegistro=false;
  }
  llamarAyudante(){
      console.log("El identity");
  console.log(this.identity);
    console.log("Esta funcionando");
            if(this.mostrarAyudante==true){
              this.mostrarAyudante=false;
            }else{
              this.mostrarAyudante=true;
            }
            if(this.mostrarAyudante){
            document.getElementById("totalAyudante").classList.remove("ayudanteSaludar");
            }else{
            document.getElementById("totalAyudante").className=document.getElementById("ayudante").className+ " ayudanteSaludar";
            

            }
  }

verificarCodigo(){
// this._userService.agregarCodigo
 this.errorCodActivacion="";
          this._userService.agregarCodigo(this.identity._id,this.identity.claveAccesoAlumno).subscribe(
          response=>{
            console.log("Aca viene la respuesta");
            console.log(response);
            let colegio=response.colegio;
          //   this.user_register=user;
            if(!colegio._id){
              this.errorCodActivacion="Error al registrarse";
            }else{
              this.identity.parametros.verificarCodAcceso=true;
              this.identity.colegio=colegio.name;
              this.errorCodActivacion="Ya eres parte del team "+ colegio.name;
              this.errorMessageRegister="Se realizo de forma exitosa el registro.Por favor proceda a loguearse";
              localStorage.setItem('identity',JSON.stringify(this.identity));
              console.log(this.identity);
            }
          // console.log("Aca viene la respuesta");
          // console.log(response); 
          },
          error=>{
            console.log("Aca viene el error");
            console.log(error);
              var errorCodActivacion=<any>error;
            if(errorCodActivacion!=null){
              var body=JSON.parse(error._body);4
              this.errorCodActivacion=body.message;
            }
          }


        );




}
        generarCodigo(){
            // console.log("Veamos que hay aca ");
            // console.log(this.identity);
            
            this._userService.generarCodigo(this.identity._id).subscribe(
                response=>{
                    console.log("LLego una respuesta");
                    console.log(response);
                    this.identity.codigoColegio=response.codigoColegio;
                    localStorage.setItem('identity',JSON.stringify(this.identity));
                    this.codigoGenerado=response.codigoColegio;
                    this.mostrarCodigo=true;
                },
                error=>{
                    console.log("Hubo error");



                })


        }
              irAcanvas(){
              console.log("Nos fuimos a canvas");
              this._router.navigate(['/ingsis']);
              this.cambiarSideBar();
              }

}
