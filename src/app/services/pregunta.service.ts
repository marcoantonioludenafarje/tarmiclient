import{Injectable} from '@angular/core';
import{Http,Response,Headers,RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map' ;
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';
import{Artist} from '../models/artist'
import{Album} from '../models/album';
import{PreguntaTestGeneral} from '../models/preguntaTestGeneral';

@Injectable()
export class PreguntaService{
    public url:string;
    constructor(private _http:Http){
        this.url=GLOBAL.url;
            }
    getPreguntas(token,page){
        let headers=new Headers({
           'Content-type':'application/json',
           'Authorization':token
        });
        let options=new RequestOptions({headers:headers});
        return this._http.get(this.url+'preguntas/'+page,options)
                .map(res=>res.json());

    }
    getPreguntasTest(token){
        let headers=new Headers({
           'Content-type':'application/json',
           'Authorization':token
        });
        let options=new RequestOptions({headers:headers});
        return this._http.get(this.url+'preguntasTest/',options)
                .map(res=>res.json());

    }
    addPregunta(token,pregunta:PreguntaTestGeneral){
        console.log("Esta funcionando el servicio de parte del fronted");
        let params=JSON.stringify(pregunta);
        let headers=new Headers({
           'Content-type':'application/json',
           'Authorization':token
        })
        return this._http.post(this.url+'savePregunta/',params,{headers:headers})
        .map(res=>res.json());
    }
    getPregunta(token ,preguntaId){
        let headers=new Headers({
           'Content-type':'application/json',
           'Authorization':token
        });
        let options=new RequestOptions({headers:headers});
        return this._http.get(this.url+'pregunta/'+preguntaId,options)
                .map(res=>res.json());

    }

    editPregunta(token,id:string,pregunta:PreguntaTestGeneral){
        let params=JSON.stringify(pregunta);
        let headers=new Headers({
           'Content-type':'application/json',
           'Authorization':token
        })
        return this._http.put(this.url+'pregunta/'+id,params,{headers:headers})
        .map(res=>res.json());
    }
    deletePregunta(token,id:string){
        let headers=new Headers({
           'Content-type':'application/json',
           'Authorization':token
        });
        let options=new RequestOptions({headers:headers});
        return this._http.delete(this.url+'pregunta/'+id,options)
        .map(res=>res.json());
    }


}