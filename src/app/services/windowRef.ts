import { Injectable } from '@angular/core';
function _window():any {
  return window;
}

@Injectable()
export class WindowRef {
    public static get():any {
        return _window();
    }
}