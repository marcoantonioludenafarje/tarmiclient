import{Injectable} from '@angular/core';
import{Http,Response,Headers,RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map' ;
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';
import{TestResultado,CheckExplFrec,CheckyDescrip} from '../models/testResultado';



@Injectable()
export class UserService{
    public url:string;
    public identity;
    public token;
    public urlFacebook="https://graph.facebook.com/me?fields=id,name,birthday,education,email,location,context,likes,first_name,last_name,gender,photos{album}";
    constructor(private _http:Http){
        this.url=GLOBAL.url;
            }
                signup(user_to_login,gethash=null){
                    console.log("Entro al servicio y veremos que trae user tod login");
                    console.log(user_to_login);
                    if(gethash !=null){
                        console.log("capta que el gethash no es null");
                        user_to_login.gethash=true;
                    }
                    let json=JSON.stringify(user_to_login);
                    let params=json;
                    let headers=new Headers({'Content-type':'application/json'});
                    return this._http.post(this.url+'login',params,{headers:headers})
                    .map(res=>res.json());
                }
                signupFacebook(authenticacion,datosBasicos,gethash=null){
                    console.log("Entro al servicio que hare el registro si es la primera vez o hara");
                    console.log(authenticacion);
                    console.log(datosBasicos);
                    

                    // Se procede a verificar si los datos de authenticacion son reales,
                    // si ha sido creado antes su perfil o si es un perfil nuevo
                    // Validamos los datos 

                        // return "Soy Feliz";
                        //si es un perfil nuevo
                        let datosNecesarios={authenticacion:authenticacion,datosBasicos:datosBasicos,gethash:null};
                        if(gethash !=null){
                        datosNecesarios.gethash=true;
                        }
                        let json=JSON.stringify(datosNecesarios);
                        console.log(json);
                        let params=json;
                        let headers=new Headers({'Content-type':'application/json'});
                        return this._http.post(this.url+'loginFacebook',params,{headers:headers})
                        .map(res=>res.json());                 

                    //si es un perfil nuevo


                }

                register(user_to_register,tipoUsuario){
                    console.log("Entro a user_to_register");
                    let params=JSON.stringify(user_to_register);
                    let headers=new Headers({'Content-type':'application/json'});
                    return this._http.post(this.url+'register/'+tipoUsuario,params,{headers:headers})
                    .map(res=>res.json());
                }


                getIdentity(){
                    let identity=JSON.parse(localStorage.getItem('identity'));
                    if(identity !="undefined"){
                        this.identity=identity;
                    }else{
                        this.identity=null;
                    }
                    return this.identity;
                }


                getToken(){
                    let token=localStorage.getItem('token');
                  
                    if(token !="undefined"){
                        this.identity=token;
                    }else{
                        this.identity=null;
                    }
                    return this.identity;



                }

                updateUser(user_to_update){
                    let params= JSON.stringify(user_to_update);
                    let headers=new Headers({
                        'Content-Type':'application/json',
                        'Authorization':this.getToken()
                    });
                    return this._http.put(this.url+'update-user/'+user_to_update._id,params,{headers:headers})
                        .map(res=> res.json());
                }
                addResult(user_to_update){
                    let params=JSON.stringify(user_to_update);
                    let headers=new Headers({
                    'Content-type':'application/json',
                    'Authorization':this.getToken()
                    })
                    return this._http.put(this.url+'addResultado',params,{headers:headers})
                    .map(res=>res.json());
                }
                getUsers(page){
                    let headers=new Headers({
                        'Content-Type':'application/json',
                        'Authorization':this.getToken()
                    });
                    let options=new RequestOptions({headers:headers});
                    return this._http.get(this.url+'users/'+page,options)
                    .map(res=>res.json());



                }
                generarCodigo(codigoColegio){
                    let headers=new Headers({
                        'Content-Type':'application/json',
                        'Authorization':this.getToken()
                    });
                    let options=new RequestOptions({headers:headers});
                    return this._http.get(this.url+'users/genCod/'+codigoColegio,options)
                    .map(res=>res.json());

                }
                agregarCodigo(user_id,codigoActivacion){
                    let params=JSON.stringify({user_id:user_id,codiAct:codigoActivacion});
                    let headers=new Headers({'Content-type':'application/json','Authorization':this.getToken()});
                    return this._http.post(this.url+'agregarCodigo/'+user_id,params,{headers:headers})
                    .map(res=>res.json());
                }
                estudiantesxColegio(ColegioId){
                    // let headers=new Headers({
                    //     'Content-Type':'application/json',
                    //     'Authorization':this.getToken()
                    // });
                    // let options=new RequestOptions({headers:headers});
                    // return this._http.get(this.url+'estudiantesxColegio/'+ColegioId,options)
                    // .map(res=>res.json());
                    let headers=new Headers({
                        'Content-Type':'application/json',
                        'Authorization':this.getToken()
                    });
                    let options=new RequestOptions({headers:headers});
                    return this._http.get(this.url+'estudiantesxColegio/'+ColegioId,options)
                    .map(res=>res.json());

                }

}
