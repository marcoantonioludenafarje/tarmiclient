import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral} from '../models/preguntaTestGeneral';
import{PreguntaService} from '../services/pregunta.service';
import{Product} from '../models/model';
import{TestResultado,CheckExplFrec,CheckyDescrip} from '../models/testResultado';
import Chart from 'chart.js';
import {Personalidades} from '../services/personalidades';
@Component({
    selector:'mis-test',
    templateUrl:'../views/mis-oficial.html',
    providers:[UserService,PreguntaService]
})
export class MisTestOficialComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public identity;
        public user:User;
        public token;
        public url:string;
        public alertMessage;
        public preguntas=[];
        public preguntaActiva;//id pregunta
        public preguntaActual;
        public testResultado:TestResultado;
        public carreras=["Ingenieria Ambiental","Ingenieria de Sistemas","Ingenieria Industrial"];
        public numPag;
        public mostrarParte=false;
        public circulos=[];
        public mostrarCirculos=false;
        public siguiente="siguiente";
        public messageMostrar:boolean;
        public empezamosTest:boolean=false;
        public checkeado:boolean=true;
        public alertCompletar=false;
        public intermedio=true;
        public mostarSkills=false;
        public resultados:any;
        public ind=0;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Mis test';
            this.identity=this._userService.getIdentity();
            this.user =this.identity;
            // this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.artist=new Artist('','','');
            this.testResultado=new TestResultado();
            // this.user.puntajeSkills=[];
            this.numPag=0;
            this.resultados=0;
            this.messageMostrar=false;
            this.testResultado.puntajeSkills=[
                {nombreSkill:"realista",puntaje:0,num:0},
                {nombreSkill:"investigador",puntaje:0,num:1},
                {nombreSkill:"artista",puntaje:0,num:2},
                {nombreSkill:"social",puntaje:0,num:3},
                {nombreSkill:"emprendedor",puntaje:0,num:4},
                {nombreSkill:"convencional",puntaje:0,num:5}
            ];
            
        }
      
        ngOnInit(){
            console.log("Veamos que tenemos aca ");
            console.log(this.identity);
            console.log('Mis -test.component.ts cargado');
            this.getPreguntas();
        }
        public trackByIndex(index: number, item) {
        return index;
        }
        getPreguntas(){

                this._preguntaService.getPreguntasTest(this.token).subscribe(
                   response=>{
                        if(!response.preguntas){
                            this._router.navigate(['/']);
                        }else{
                            this.preguntas=response.preguntas;
                            // console.log("aca viene mis preguntas");
                            // console.log(this.preguntas);
                            this.preguntaActual=0;
                            this.preguntaActiva=this.preguntas[this.preguntaActual];      
                        }
                    },
                    error=>{
                        var errorMessage=<any>error;
                        if(errorMessage!=null){
                            var body=JSON.parse(error._body);
                            // this.errorMessage=body.message;
                            console.log(error);
                        }

                    }


                );
        }
   focusInput(event){
    // console.log(event);
    event.srcElement.parentElement.children[1].className="label active";
    // console.log("aca viene algo bien chido");
    // console.log(event.srcElement.parentElement.children[0].className);
    event.srcElement.parentElement.children[0].className=event.srcElement.parentElement.children[0].className.replace("error","")

  }
  blurInput(event){
    // console.log(event);
    if(event.srcElement.value<=0){
    event.srcElement.parentElement.children[1].className="label";
    event.srcElement.parentElement.children[0].className=event.srcElement.parentElement.children[0].className + " error";
    }

  }
    empezarTest(){

    this.empezamosTest=true;
    document.getElementById("despuesMensaje").classList.remove("desaparecer");
    document.getElementById(this.preguntaActiva._id).classList.remove("ocultoElegante");
    // document.getElementById(this.preguntaActiva._id).className=document.getElementById(this.preguntaActiva._id).className + " preguntaActiva"; 
    var tamanoPantalla=window.innerHeight;
    var tamanoVerticalBar=document.getElementById("navigation-vertical").clientHeight;
    var tamanoContenedor=tamanoPantalla-tamanoVerticalBar;
    document.getElementById("cuerpo-mi-test").style.height=tamanoContenedor.toString()+"px";
    }           
    adelante(){
    var cantChecked=0;
    for (var i = 0; i < this.preguntaActiva.alternativas.length; i++) {
    var cosas:any=document.getElementById(this.preguntaActiva.alternativas[i]._id);
    if(cosas.checked==true){
        cantChecked++;
    }
    }
    if(cantChecked>0){
    this.preguntaActual++;
    this.preguntaActiva=this.preguntas[this.preguntaActual];
    var numeroAlter=this.preguntaActual*100;
    for (var i = 0; i < this.preguntas.length; i++) {
     document.getElementById(this.preguntas[i]._id).style.transform="translateX(-"+numeroAlter.toString()+"%)"   
        }
        document.getElementById("resultadosTest").style.transform="translateX(-"+numeroAlter.toString()+"%)"; 
    document.getElementsByClassName("circulo-interno")[this.preguntaActual-1].className=document.getElementsByClassName("circulo-interno")[0].className+" circuloActiva";
        this.alertCompletar=false;
    document.getElementById("siguiente-preg").classList.remove("botonSigActivado"); 
    if(this.preguntaActual==this.preguntas.length-1){
    var auxiliar:any=document.getElementById("siguiente-preg");
    auxiliar.value="Ver Resultado";
    }
    if(this.preguntaActual==this.preguntas.length){
    for (var i = 0; i < this.preguntas.length; i++) {
    var miLista=document.getElementById(this.preguntas[i]._id).getElementsByTagName("input");
    for (var j = 0; j < miLista.length; j++) {
        if(miLista[j].checked==true){
        var result = this.preguntas[i].alternativas.filter(function( obj ) {
        return obj._id == miLista[j].id;
        });
        // console.log(result);
        for (var k = 0; k < result[0].valorSkills.length; k++) {
        this.testResultado.puntajeSkills[k].puntaje=this.testResultado.puntajeSkills[k].puntaje + result[0].valorSkills[k].puntos;
        this.testResultado.puntajeSkills[k].puntaje=parseFloat(this.testResultado.puntajeSkills[k].puntaje.toFixed(2));
        }
        }
    }
    }
    this.user.testResultado=this.testResultado;
    this._userService.addResult(this.user).subscribe(
                   response=>{
                        if(!response.testResultado){
                            alert("No recibio el resultado esperado");
                        }else{
                            console.log("Aca viene algunas cosas de mis respuestas");
                            console.log(response);
                            this.user.testResultado=response.testResultado;
                            this.mostarSkills=true;
                            document.getElementById("cuerpo-mi-test").className=document.getElementById("cuerpo-mi-test").className+ " desaparecer";
                            document.getElementById("cuerpo-mis-Skills").classList.remove("desaparecer");
                            this.crearResultados(); 
                        }
                    },
                    error=>{
                        var errorMessage=<any>error;
                        if(errorMessage!=null){
                            var body=JSON.parse(error._body);
                            // this.errorMessage=body.message;
                            console.log(error);
                        }

                    }


                )
    }
    }else{
    this.alertCompletar=true;  
    }

    }
    atras(){
    if(this.preguntaActual>0){
    this.preguntaActual=this.preguntaActual-1;
    this.preguntaActiva=this.preguntas[this.preguntaActual];
    var numeroAlter=this.preguntaActual*100;
    for (var i = 0; i < this.preguntas.length; i++) {
     document.getElementById(this.preguntas[i]._id).style.transform="translateX(-"+numeroAlter.toString()+"%)"   
        }
    document.getElementById("resultadosTest").style.transform="translateX(-"+numeroAlter.toString()+"%)"; 
    document.getElementsByClassName("circulo-interno")[this.preguntaActual].classList.remove("circuloActiva");
    if(this.preguntaActual<this.preguntas.length-1){
    var auxiliar:any=document.getElementById("siguiente-preg");
    auxiliar.value="Continuar";
    }  
    
    }else{
        console.log("Que pendejin eres");
        }       
    }
    habilitarSig(){
    var cantChecked=0;
    for (var i = 0; i < this.preguntaActiva.alternativas.length; i++) {
    var cosas:any=document.getElementById(this.preguntaActiva.alternativas[i]._id);
    if(cosas.checked==true){
        cantChecked++;
    }
    }
    if(cantChecked>0){
    document.getElementById("siguiente-preg").className=document.getElementById("siguiente-preg").className+" botonSigActivado";
    }else{
    document.getElementById("siguiente-preg").classList.remove("botonSigActivado"); 
    }

    }
    hacerGraficaRadar(){
        var ctx4 = document.getElementById("myChart3");
        
        var myChart4 = new Chart(ctx4,
            {
                            type: 'radar',
                            data: {
                                labels: ["Realista","Investigador", "Artista","Social", "Emprendedor", "Convencional"],
                                datasets: [{
                                    label: "Mi resultado",
                                    backgroundColor:'#d7ecfb',
                                    borderColor:'#45a9ec',
                                    pointBackgroundColor:'#45a9ec',
                                    data: [this.testResultado.puntajeSkills[0].puntaje,this.testResultado.puntajeSkills[1].puntaje,this.testResultado.puntajeSkills[2].puntaje,this.testResultado.puntajeSkills[3].puntaje,this.testResultado.puntajeSkills[4].puntaje,this.testResultado.puntajeSkills[5].puntaje]
                                    
                                },]
                            },
                            options: {
                                legend: {
                                    position: 'top',
                                    display:false
                                },
                                title: {
                                    display:false,
                                    text: 'Chart.js Radar Chart'
                                },
                                scale: {
                                ticks: {
                                    beginAtZero: true
                                },
                                pointLabels: {
                                    fontSize: 20
                                }
                                },
                            }
                        }
                    );
        document.getElementById("myChart3").style.height="200px";
        document.getElementById("myChart3").style.width="200px";
        }

        crearResultados(){
        this.resultados = this.user.testResultado.resultadosPorcentajes.filter(( obj )=>{
        return obj.puntaje>19;
        });
        var suma=0;
        var auxiliar;   
        for (var i = 0; i < this.resultados.length; i++) {
        auxiliar=this.resultados[i].puntaje;
        suma=suma + auxiliar;
        }
        for (var i = 0; i <this.resultados.length; i++) {
            auxiliar=this.resultados[i].puntaje;
            // console.log(((auxiliar/suma)*100).toFixed(1))
            var text5=document.createTextNode(((auxiliar/suma)*100).toFixed(1)+"%" );
            var puntaje=(auxiliar/suma)*100 -0.5;
            //Aca construyo mi barra personalidad
            var contenedor = document.createElement("div");                      
            contenedor.style.width=puntaje.toFixed(2)+"%";
            var result:any = Personalidades.filter(( obj )=>{
            return obj.num == this.resultados[i].num;
            });
            contenedor.style.background="linear-gradient(to right,"+result[0].color.primario+", " +result[0].color.secundario+" )";
            contenedor.appendChild(text5);
            document.getElementById("barraPersonalidad").appendChild(contenedor);

            //ahora a construir mis tarjeteros
            var tarjeta=document.createElement("div");
            tarjeta.className="tarjeta";
            //haciendo la parte superior de la tarjeta
            var parteSup=document.createElement("div");
            var imgPersonalidad=document.createElement("img");
            imgPersonalidad.src="./../assets/images/"+"b"+this.resultados[i].num+"f.svg";
            var circuloPu=document.createElement("div");
            var numeroCir=document.createElement("span");
            var t = document.createTextNode((i+1).toString());
            circuloPu.className="circuloPuesto";
            numeroCir.appendChild(t);
            circuloPu.appendChild(numeroCir);
            parteSup.appendChild(imgPersonalidad);
            parteSup.appendChild(circuloPu);
            parteSup.className="parteSupTar";
            parteSup.style.background="linear-gradient(to right,"+result[0].color.primario+", " +result[0].color.secundario+" )";
            //Haciendo la parte inferior de la tarjeta
            var tituloPi=document.createElement("span");
            tituloPi.className="tituloPI";
            var cuerpoPi=document.createElement("span");
            cuerpoPi.className="cuerpoPI";
            var algunasCar=document.createElement("div");
            algunasCar.className="algunasCarreras";
            var carrerasTi=document.createElement("span");
            carrerasTi.className="carreras";
            var listaCarreras=document.createElement("div");
            listaCarreras.className="listaCarreras";
            var botonesTarjeta=document.createElement("div");
            botonesTarjeta.className="botonesTarjeta";
            var empezarBoton=document.createElement("span");
            empezarBoton.className="empezarButton";
            var t1 = document.createTextNode("Explorar Carreras");
            var t2 = document.createTextNode(result[0].nombre);
            var t3 = document.createTextNode(result[0].descripcion);
            var t4 = document.createTextNode("Carreras");
            carrerasTi.appendChild(t4);
            
            //tituloPi
            tituloPi.appendChild(t2);
            //cuerpo Pi
            cuerpoPi.appendChild(t3);
            

            for (var j = 0; j <result[0].carreras.length; j++) {
             var carrerasElm=document.createElement("span");
             var contenidoLista=document.createTextNode(result[0].carreras[j].nombre);
             carrerasElm.appendChild(contenidoLista);
             listaCarreras.appendChild(carrerasElm);
            }
            algunasCar.appendChild(carrerasTi);
            algunasCar.appendChild(listaCarreras);
            empezarBoton.appendChild(t1);
            botonesTarjeta.appendChild(empezarBoton);
            var parteInf=document.createElement("div");
            parteInf.className="parteInfTar";
            parteInf.appendChild(tituloPi);
            parteInf.appendChild(cuerpoPi);
            parteInf.appendChild(algunasCar);
            parteInf.appendChild(botonesTarjeta);
            tarjeta.appendChild(parteSup);
            tarjeta.appendChild(parteInf);


            document.getElementById("cuerpoCarr").appendChild(tarjeta);

        }
        }
        atrasCarr(){
            console.log("atras carr");
            if(this.ind>0){
            this.ind=this.ind-1;
            for (var i = 0; i <this.resultados.length; i++) {
                var auxi:any=document.getElementById("cuerpoCarr").getElementsByClassName("tarjeta")[i];
                auxi.style.transform="translateX(-"+300*this.ind +"px)"; 
            }
            }
        }
        
        sgteCarr(){
            console.log("siguiente carr");
            console.log(this.resultados);
            if(this.ind<this.resultados.length-1){
            this.ind++;
            for (var i = 0; i <this.resultados.length; i++) {
                var auxi:any=document.getElementById("cuerpoCarr").getElementsByClassName("tarjeta")[i];
                auxi.style.transform="translateX(-"+300*this.ind +"px)"; 
            }
            }
        }
}