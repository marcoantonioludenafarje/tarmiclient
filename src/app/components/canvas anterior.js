 canvas(){
        var frame:any = window.requestAnimationFrame;
        var canvas:any=document.querySelector("#lienzo");
        var ctx:any=canvas.getContext("2d");
    //     console.log("ctx",ctx);
    // //CUADROS
    //     // RELLENO
    //     ctx.fillStyle="cyan";
    //     ctx.fillRect(0,0,500,100);

    //     //CONTORNO
    //     ctx.lineWidth=5;
    //     ctx.strokeStyle="rgba(255,0,255,1)";
    //     ctx.rect(200,300,10,10); //Recorrido de ese contorno
    //     ctx.stroke();

    //     ctx.rect(400,400,10,10); //Recorrido de ese contorno
    //     ctx.stroke();
    // //CIRCULOS
    //     ctx.beginPath();
    //     //ctx.arc(x1,y1,r,startAngle,endAngle);
    //     ctx.arc(300,300,80,0,2*Math.PI);
    //     //RELLENO CIRCULO
    //     ctx.fillStyle="green";
    //     ctx.fill();
        
    //     //CONTORNO CIRCULO

    //     ctx.lineWidth=5; //XQ este contorno se le aplica a este circulo ,
    //     //Es debido a que se le ha aplicado 
    //     ctx.strokeStyle="purple";
    //     ctx.stroke();

    // //LINEAS
    //     ctx.beginPath();
    //     ctx.moveTo(0,0);
    //     ctx.lineTo(200,200);
    //     ctx.lineTo(400,200);
    //     ctx.lineTo(600,400);
    //     ctx.lineTo(800,200);
    //     ctx.lineTo(1000,200);
    //     ctx.lineTo(1000,0);
    //     //Contorno de Linea
    //     ctx.lineWidth=5;
    //     ctx.stroke();

    //     ctx.fillStyle='rgba(0,0,255,.6)';
    //     ctx.fill();
    // // CURVAS
    //     ctx.beginPath();
    //     ctx.moveTo(0,500);
    //     //ctx.bezierCurveTo(cpx1,cpy1,cpx2,cpy2,x2,y2) 
    //     ctx.bezierCurveTo(200,300,400,400,500,500); 

    //     ctx.lineWidth=5;
    //     ctx.strokeStyle='rgba(100,0,0,1)';
    //     ctx.stroke();
    //     ctx.fillStyle='red';
    //     ctx.fill();
    // //DEGRADADOS
    //     var grd=ctx.createLinearGradient(0,300,100,400);
    //     grd.addColorStop(0,"red");
    //     grd.addColorStop(1,"yellow");
    //     ctx.fillStyle=grd;
    //     ctx.fillRect(0,300,100,100);//Punto (0,300) y se mueve 100 de eje x , y 100 de eje y
        
    //     //ctx.createRadialGradient(x1,y1,r1,x2,y2,r2);
    //     var grd2=ctx.createRadialGradient(900,400,0,900,400,100);
    //     grd2.addColorStop(0,"white");
    //     grd2.addColorStop(1,"black");
    //     ctx.beginPath();
    //     ctx.arc(900,400,100,0,7);
    //     ctx.fillStyle=grd2;
    //     ctx.fill();

        //CIELO
        var alfa=1;
         var grd=ctx.createLinearGradient(0,0,0,500);
         grd.addColorStop(0,"rgba(0,0,255,"+alfa+")");
         grd.addColorStop(1,"white");
         ctx.fillStyle=grd;
         ctx.fillRect(0,0,1000,500)

         //OCEANO
         var grd2=ctx.createLinearGradient(0,400,0,500);
         grd.addColorStop(0,"rgba(0,180,255,"+alfa+")");
         grd.addColorStop(1,"white");
         ctx.fillStyle=grd;
         ctx.fillRect(0,400,1000,100)
         
         //---------//
         //MONTAÑA 1
         ctx.beginPath();
         ctx.fillStyle='rgba(100,0,255,'+alfa+')';
         ctx.moveTo(0,400);
         ctx.lineTo(200,100);
         ctx.lineTo(400,400);
         ctx.fill();
         //MONTAÑA 2
         ctx.beginPath();
         ctx.fillStyle='rgba(50,0,255,'+alfa+')';
         ctx.moveTo(200,400);
         ctx.lineTo(400,100);
         ctx.lineTo(600,400);
         ctx.fill();
         
        //MONTAÑA 3
         ctx.beginPath();
         ctx.fillStyle='rgba(100,0,255,'+alfa+')';
         ctx.moveTo(400,400);
         ctx.lineTo(600,100);
         ctx.lineTo(800,400);
         ctx.fill();

        //MONTAÑA 4
         ctx.beginPath();
         ctx.fillStyle='rgba(50,0,255,'+alfa+')';
         ctx.moveTo(600,400);
         ctx.lineTo(800,100);
         ctx.lineTo(1000,400);
         ctx.fill();
         
         //NIEVE DE MONTAÑA 1
         ctx.beginPath();
         ctx.fillStyle='rgba(255,255,255,'+alfa+')';
         ctx.moveTo(133,200);
         ctx.lineTo(200,100);
         ctx.lineTo(200,300);
         ctx.fill();
        //NIEVE DE MONTAÑA 1
         ctx.beginPath();
         ctx.fillStyle='rgba(210,210,255,'+alfa+')';
         ctx.moveTo(200,300);
         ctx.lineTo(200,100);
         ctx.lineTo(267,200);
         ctx.fill();


        //NIEVE DE MONTAÑA 2 
         ctx.beginPath();
         ctx.fillStyle='rgba(255,255,255,'+alfa+')';
         ctx.moveTo(333,200);
         ctx.lineTo(400,100);
         ctx.lineTo(400,300);
         ctx.fill();
        //NIEVE DE MONTAÑA 2
         ctx.beginPath();
         ctx.fillStyle='rgba(210,210,255,'+alfa+')';
         ctx.moveTo(400,300);
         ctx.lineTo(400,100);
         ctx.lineTo(467,200);
         ctx.fill();
        //  ctx.lineTo

        //NIEVE DE MONTAÑA 3 
         ctx.beginPath();
         ctx.fillStyle='rgba(255,255,255,'+alfa+')';
         ctx.moveTo(533,200);
         ctx.lineTo(600,100);
         ctx.lineTo(600,300);
         ctx.fill();
        //NIEVE DE MONTAÑA 3
         ctx.beginPath();
         ctx.fillStyle='rgba(210,210,255,'+alfa+')';
         ctx.moveTo(600,300);
         ctx.lineTo(600,100);
         ctx.lineTo(667,200);
         ctx.fill();

        //NIEVE DE MONTAÑA 4
         ctx.beginPath();
         ctx.fillStyle='rgba(255,255,255,'+alfa+')';
         ctx.moveTo(733,200);
         ctx.lineTo(800,100);
         ctx.lineTo(800,300);
         ctx.fill();
        //NIEVE DE MONTAÑA 4
         ctx.beginPath();
         ctx.fillStyle='rgba(210,210,255,'+alfa+')';
         ctx.moveTo(800,300);
         ctx.lineTo(800,100);
         ctx.lineTo(867,200);
         ctx.fill();

        //MONTES 1
         ctx.beginPath();
         ctx.fillStyle='rgba(10,0,150,'+alfa+')';
         ctx.moveTo(0,400);
         ctx.lineTo(200,300);
         ctx.lineTo(200,450);
         ctx.fill();

         ctx.beginPath();
         ctx.fillStyle='rgba(10,0,150,'+alfa+')';
         ctx.moveTo(400,400);
         ctx.lineTo(200,300);
         ctx.lineTo(200,450);
         ctx.fill();
        
         //MONTES 2
         ctx.beginPath();
         ctx.fillStyle='rgba(10,0,150,'+alfa+')';
         ctx.moveTo(600,400);
         ctx.lineTo(800,300);
         ctx.lineTo(800,450);
         ctx.fill(); 
         
         ctx.beginPath();
         ctx.fillStyle='rgba(10,0,150,'+alfa+')';
         ctx.moveTo(1000,400);
         ctx.lineTo(800,300);
         ctx.lineTo(800,450);
         ctx.fill();

        //ARBOL 1 
        ctx.fillStyle='rgba(100,0,20,1)';
        ctx.fillRect(100,200,20,150);//Se ubica en el punto (100,200) y se tiene 20 puntos a la derecha y 150 puntos hacia abajo

        ctx.beginPath();
        ctx.fillStyle='rgba(0,140,25,'+alfa+')';
        ctx.arc(140,180,40,0,2*Math.PI);
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle='rgba(0,110,25,'+alfa+')';
        ctx.arc(80,190,30,0,2*Math.PI);
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle='rgba(0,80,25,'+alfa+')';
        ctx.arc(110,200,50,0,2*Math.PI);
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle='rgba(0,50,25,'+alfa+')';
        ctx.arc(120,230,30,0,2*Math.PI);
        ctx.fill();
        
        var imagenSVG=new Image();
        imagenSVG.src="../../assets/images/tata.svg";
        imagenSVG.onload=function(){
            ctx.drawImage(imagenSVG,700,100,imagenSVG.naturalWidth,imagenSVG.naturalHeight)
        }
        var numero=0;
        var ubicacionX=0;
        var animacion;
        var sprite =new Image();
        sprite.src="../../assets/images/opcion1.png";
        function tiempo(){
            if(numero>=600){
                numero=0;
            }else{
                numero+=30;
            }
            for(var i=0; i<600; i+=100){
                if(numero>=i){
                    ubicacionX=i
                }
            }
            console.log(numero);
            ctx.clearRect(0,0,canvas.width,canvas.height);
            ctx.drawImage(sprite,ubicacionX,0,100,100,0,100,100,100);
            //ctx.drawImage(imagen,ubicacionX,ubicacionY,recorteX,recorteY,x1,y1,x2,y2);
            //Ubicacion x  ubicacion y.-Es respecto a la imagen #1 del fragmento 
            //Recorte x , recorte Y.-Respecto al punto x,y ...se movera una cantidad en el eje x y otra en el eje y. 
            //x, y  es nuestro punto (0,100) y de ahi se mueve 100 de largo y de ancho 
            animacion=frame(tiempo);
        }
        tiempo();
        setTimeout(function(){
            cancelAnimationFrame(animacion);
        },3000)

        }