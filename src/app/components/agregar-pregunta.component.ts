import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral,Alternativas,SkillBasico} from '../models/preguntaTestGeneral';
import {Personalidad} from '../models/personalidad';

//naturales
import { Product } from '../models/model';
@Component({
    selector:'editar-test',
    templateUrl:'../views/agregar-pregunta.html',
    providers:[UserService,PreguntaService]
})
export class AgregarPreguntaComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public preguntaPrueb:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        public skills=["realista","investigador","artista","social","emprendedor","convencional"];
        public  names: string[];
        public numAlternativa:number;
        public pruebitas=[{nombre:"Marco Ludeña",apellido:"Farje"},{nombre:"Juan",apellido:"Camacho"},{nombre:"Cosito",apellido:"Farjesino"}];
        public alertMessage;
        public tiposPreguntas=["Una sola es correcta","Todas las alternativas son validas"];
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Crear Pregunta';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.pregunta=new PreguntaTestGeneral('','');
            this.preguntaPrueb=new PreguntaTestGeneral('','');
            this.pregunta.alternativas=[];
            this.numAlternativa=0;
            this.initValoresAlternativas(this.numAlternativa);
            this.names = ['a', 'b', 'c'];
        }
                public trackByIndex(index: number, item) {
                return index;
                }
      
        ngOnInit(){
 
            console.log('Editar-test.component.ts cargado');

        }    

        onSubmit(){
            // console.log(this._preguntaService.addPregunta(this.token,this.pregunta));
            // this.pregunta.fechaCreacion=new Date();
            for(var i=0;i<this.pregunta.alternativas.length;i++){
                var suma=0;
                for(var j=0;j<this.pregunta.alternativas[i].valorSkills.length;j++){
                    suma=suma+this.pregunta.alternativas[i].valorSkills[j].puntos;
                }
                console.log(suma);
                for(var j=0;j<this.pregunta.alternativas[i].valorSkills.length;j++){
                    // var num=this.pregunta.alternativas[i].valorSkills[j].puntos/suma;
                    // console.log(num.toFixed(2));
                    this.pregunta.alternativas[i].valorSkills[j].puntos=Number((this.pregunta.alternativas[i].valorSkills[j].puntos/suma).toFixed(2));
                    // console.log(Number(num.toFixed(2)));
                    // console.log(this.pregunta.alternativas[i].valorSkills[j].puntos);
                }               

            }
            console.log("Aca viene al bien prro");
            console.log(this.pregunta);
            
            this._preguntaService.addPregunta(this.token,this.pregunta).subscribe(
            response=>{
                console.log(response);
                if(!response.pregunta){
                    alert("Error en el servidor");
                    this.alertMessage="Error en el servidor";
                }else{
                    this.pregunta=response.pregunta;
                    console.log(this.pregunta);
                    // this._router.navigate(['/editar-artista',response.artist._id]);
                    this.alertMessage="La pregunta se ha creado correctamente";
                    window.alert("La pregunta se ha creado correctamente");
                    this._router.navigate(['/configurar-test',1]);
                }
            },
            error=>{
                    var errorMessage=<any>error;
                    if(errorMessage!=null){
                        var body=JSON.parse(error._body);
                        // this.errorMessage=body.message;
                        console.log(error);
                    }
            });

        }

        initValoresAlternativas(numAlternativa){
            let valoresBasicos=[];
            for (let i = 0; i < this.skills.length; i++) {
                    valoresBasicos.push({
                        skillBasico:this.skills[i],
                        puntos:0
                    });
                }
            this.pregunta.alternativas.push({
                ordenAlternativa:numAlternativa+1,
                cuerpoAlternativa:"",
                valorSkills:valoresBasicos
            });
        }
    

        agregarAlternativa(){
            console.log("Hola amiguitos");
            this.numAlternativa=this.pregunta.alternativas.length;
            this.initValoresAlternativas(this.numAlternativa);

        }
        eliminarAlternativa(numAlt:number){
            console.log("Hola BOLI");
            console.log(numAlt);
            this.pregunta.alternativas.splice(numAlt, 1);
            console.log(this.pregunta.alternativas);
            let cantPreguntas=this.pregunta.alternativas.length;
            for (let i = 0; i < cantPreguntas; i++) {
            this.pregunta.alternativas[i].ordenAlternativa=i+1;
            }

        }

        eliminarAlt(numAlt:number){
          console.log(this.pruebitas);
          if(this.pruebitas.length > 0) this.pruebitas.splice(numAlt, 1);
                //       for (let i = 0; i < this.skills.length; i++) {
                //     valoresBasicos.push({
                //         skillBasico:this.skills[i],
                //         puntos:0
                //     });
                // }
        }
    




}