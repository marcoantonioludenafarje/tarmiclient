import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral,Alternativas,SkillBasico} from '../models/preguntaTestGeneral';
import Chart from 'chart.js';
import {Personalidades} from '../services/personalidades';

//naturales
import { Product } from '../models/model';
@Component({
    selector:'editar-test',
    templateUrl:'../views/panel-control-alumnos.html',
    providers:[UserService,PreguntaService]
})
export class PanelControlAlumnosComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public preguntaPrueb:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        public porcentajesSalon;
        public pagActualppa=1;
        public cantidadxTabla=5;
        public estudianteppa;
        public estudianteppaparcial=[];
        // public mostrarLista=false;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Panel de control';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
        }
                public trackByIndex(index: number, item) {
                return index;
                }
      
        ngOnInit(){
            console.log('Panel de control');
            this.estudiantesxColegio();
        }
        
//             hacerGraficaRadar(){
//         var ctx4 = document.getElementById("bar-chart");
        
// new Chart(document.getElementById("bar-chart"), {
//     type: 'bar',
//     data: {
//       labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
//       datasets: [
//         {
//           label: "Population (millions)",
//           backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
//           data: [2478,5267,734,784,433]
//         }
//       ]
//     },
//     options: {
//       legend: { display: false },
//       title: {
//         display: true,
//         text: 'Predicted world population (millions) in 2050'
//       }
//     }
// });
// new Chart(document.getElementById("doughnut-chart"), {
//     type: 'doughnut',
//     data: {
//       labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
//       datasets: [
//         {
//           label: "Population (millions)",
//           backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
//           data: [2478,5267,734,784,433]
//         }
//       ]
//     },
//     options: {
//       title: {
//         display: true,
//         text: 'Predicted world population (millions) in 2050'
//       }
//     }
// });
                    
//         // document.getElementById("myChart3").style.height="200px";
//         // document.getElementById("myChart3").style.width="200px";
//         }


estudiantesxColegio(){
  console.log("Veamos que viene");
  console.log(this.identity);
  this._userService.estudiantesxColegio(this.identity._id).subscribe(
                   response=>{
                    if(response){
                      console.log("Mi respuesta");
                      console.log(response);
                      var personalidades=[];
                      var analisisAlum=[];
                      var colores=[];
                      this.estudianteppa=[];
                      for (var i = 0; i < Personalidades.length; i++) {
                         personalidades.push(Personalidades[i].nombre);
                         colores.push(Personalidades[i].color.primario);
                         analisisAlum.push({principal:0,totales:0});
                      }
                      for (var i = 0; i < response.alumnos.length; i++) {
                        analisisAlum[response.alumnos[i].testResultado.resultadosPorcentajes[0].num].principal++;
                        this.estudianteppa.push({nombre:response.alumnos[i].name,sexo:response.alumnos[i].sexo,gradoEstudio:response.alumnos[i].gradoEstudio});
                        for (var j = 0; j < response.alumnos[i].testResultado.resultadosPorcentajes.length; j++) {
                          if(response.alumnos[i].testResultado.resultadosPorcentajes[j].puntaje>=20){
                           analisisAlum[response.alumnos[i].testResultado.resultadosPorcentajes[j].num].totales++; 
                          }
                        }
                      }
                      for (var i = 0; i <this.cantidadxTabla; i++) {
                        this.estudianteppaparcial.push(this.estudianteppa[i]);
                      }
                      // this.mostrarLista=true;
                      new Chart(document.getElementById("pieAlumnos"), {
                          type: 'pie',
                          data: {
                            labels: personalidades,
                            datasets: [
                              {
                                label: "Cantidad de interesados",
                                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                                data: [analisisAlum[0].principal,analisisAlum[1].principal,analisisAlum[2].principal,analisisAlum[3].principal,analisisAlum[4].principal,analisisAlum[5].principal]
                              }
                            ]
                          },
                          options: {
                            legend: { display: false },
                            title: {
                              display: true,
                              text: 'Personalidad Predominante de mis alumnos'
                            },
                            maintainAspectRatio: false
                          }
                      });

                      new Chart(document.getElementById("barAlumnos"), {
                          type: 'bar',
                          data: {
                            labels: personalidades,
                            datasets: [
                              {
                                label: "Cantidad de alumnos",
                                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                                data: [analisisAlum[0].totales,analisisAlum[1].totales,analisisAlum[2].totales,analisisAlum[3].totales,analisisAlum[4].totales,analisisAlum[5].totales]
                              }
                            ]
                          },
                          options: {
                            legend: { display: false },
                            title: {
                              display: true,
                              text: 'Intereses vocacionales'
                            },
                            maintainAspectRatio: false,
                            scales: {
                            yAxes: [{
                              ticks: {
                                beginAtZero: true,
                                min: 0
                              }    
                            }]
                          }
                          }
                      });


                    }else{
                      console.log("Hubo un error en la peticion");
                    }
                    },
                    error=>{
                        // var errorMessage=<any>error;
                        // if(errorMessage!=null){
                        //     var body=JSON.parse(error._body);
                        //     // this.errorMessage=body.message;
                        //     console.log(error);
                        // }
                          console.log(error);
                    });
}
atrasppa(){
  if(this.pagActualppa>1){
    this.pagActualppa=this.pagActualppa-1;
    console.log(this.pagActualppa);
    this.estudianteppaparcial=[];
    for (var i = (this.pagActualppa-1)*this.cantidadxTabla; i <this.pagActualppa*this.cantidadxTabla; i++) {
        this.estudianteppaparcial.push(this.estudianteppa[i]);
     }
  }
}
siguienteppa(){
  var cantPaginas=Math.trunc(this.estudianteppa.length/this.cantidadxTabla);

  if(this.estudianteppa.length%this.cantidadxTabla>0){
    cantPaginas++;
  }
  if(this.pagActualppa<cantPaginas){
    this.pagActualppa++;
    this.estudianteppaparcial=[];
    for (var i = (this.pagActualppa-1)*this.cantidadxTabla; i <this.pagActualppa*this.cantidadxTabla; i++) {
        this.estudianteppaparcial.push(this.estudianteppa[i]);
     }
  }


}

veamosLaVerdad(){
    this._userService.estudiantesxColegio(this.identity._id).subscribe(
      response=>{
        console.log(response);






      },error=>{
        console.log(error);
      }
    );
}
}