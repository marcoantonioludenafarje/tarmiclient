import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral,Alternativas,SkillBasico} from '../models/preguntaTestGeneral';
import Chart from 'chart.js';

//naturales
import { Product } from '../models/model';
@Component({
    selector:'editar-test',
    templateUrl:'../views/administracion-usuarios.html',
    providers:[UserService,PreguntaService]
})
export class AdministracionUsuariosComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public preguntaPrueb:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        public porcentajesSalon;
        public usuarios:any;
        public nuevoUsuario:boolean;
        public pagUsuario=1;
        public numUsuarios;
        public codigoGenerado="";
        public mostrarCodigo=false;
        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Panel de control de Usuarios';
            this.identity=this._userService.getIdentity();
            this.token=this._userService.getToken();
            this.url=GLOBAL.url;
            this.nuevoUsuario=false;
        }
                public trackByIndex(index: number, item) {
                return index;
                }
      
        ngOnInit(){
            console.log("Aca viene nuestro usuario");
            console.log(this.identity);
            console.log('Panel de control');
            this.getUsers(this.pagUsuario);

        }
        getUsers(paginaUsuario){
             this._userService.getUsers(this.pagUsuario).subscribe(
                   response=>{
                        console.log("Aca viene nuestra respuesta");
                        console.log(response);
                        if(!response){
                            alert("No recibio el resultado esperado");
                        }else{
                            // console.log("Supuestamente si hay respuesta");
                            this.usuarios=response;
                            this.numUsuarios=response.total_items;
                        }
                    },
                    error=>{
                        var errorMessage=<any>error;
                        if(errorMessage!=null){
                            // var body=JSON.parse(error._body);
                            // this.errorMessage=body.message;
                            console.log(error);
                        }

                    }
                )   
        }
        anteriorPage(){
        if(this.pagUsuario>1){
            this.pagUsuario=this.pagUsuario-1;
            this.getUsers(this.pagUsuario);
        }else{
            this.pagUsuario=1
        }
        }
        despuesPage(){ 
            if(this.pagUsuario<((this.numUsuarios-(this.numUsuarios%3))/3)+1){
                this.pagUsuario++;
                this.getUsers(this.pagUsuario);
            }else{
                console.log("Lo siento llego al limite");
            }
        }
        eliminarUsuario(){
            console.log("Se va a eliminar un usuario");
        }
        editarUsuario(){
            console.log("Se va a editar un usuario");
        }
        guardarUsuario(){
            console.log("Se va a guardar un usuario");
        }
        


        activarNuevoUsuario(){
            this.nuevoUsuario=true;
        }
        generarCodigo(){
            // console.log("Veamos que hay aca ");
            // console.log(this.identity);
            
            this._userService.generarCodigo(this.identity._id).subscribe(
                response=>{
                    console.log("LLego una respuesta");
                    console.log(response);
                    this.codigoGenerado=response.codigoColegio;
                    this.mostrarCodigo=true;
                },
                error=>{
                    console.log("Hubo error");



                })


        }



}