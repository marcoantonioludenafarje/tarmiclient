import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral,Alternativas,SkillBasico} from '../models/preguntaTestGeneral';
import Chart from 'chart.js';

//naturales
import { Product } from '../models/model';
@Component({
    selector:'editar-test',
    templateUrl:'../views/ingenieriasistemas.html',
    providers:[UserService,PreguntaService]
})
export class Ingsistemascomponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public preguntaPrueb:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        public ampliarPa:String="Ampliar";

        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Juegos Ingeniería de Sistemas';
        }
        ngOnInit(){

            var auxiliar=document.getElementById("cuerpoCentral");
            var barraHorizontal=document.getElementById("navigation-vertical");
            auxiliar.style.margin="0px";
            auxiliar.style.padding="0px";
            auxiliar.style.width="100vw";
            barraHorizontal.style.display="none";
            document.getElementById("rechargePart").className=document.getElementById("rechargePart").className+ " cuerpoExteriorJuegos";
            this.canvasCreacion();
        }

        ampliar(){
            if(this.ampliarPa=="Ampliar"){
            // var auxiliar=document.getElementById("cuerpoCentral");
            // var barraHorizontal=document.getElementById("navigation-vertical");
            var contenedor:any=document.querySelector("#contenedor-juegos");
            var ampliarLienzo:any=document.querySelector("#lienzo");
            var btnAmpliar:any=document.querySelector("#btnAmpliar");


            contenedor.style.width="100%";
            contenedor.style.height="100vh";
            contenedor.style.margin="0";

            ampliarLienzo.style.width="100%";
            ampliarLienzo.style.height="100vh";
            ampliarLienzo.style.backgroundSize="cover";
            ampliarLienzo.style.backgroundRepeat="no-repeat";

            btnAmpliar.style.position="fixed";
            btnAmpliar.style.top="10px";
            btnAmpliar.style.left="10px";
            btnAmpliar.style.zIndex="1";

            // auxiliar.style.margin="0px";
            // auxiliar.style.padding="0px";
            // auxiliar.style.width="100vw";

            // barraHorizontal.style.display="none";

            this.ampliarPa="Reducir";
            }else{
            var auxiliar=document.getElementById("cuerpoCentral");
            var barraHorizontal=document.getElementById("navigation-vertical");
            var contenedor:any=document.querySelector("#contenedor-juegos");
            var ampliarLienzo:any=document.querySelector("#lienzo");
            var btnAmpliar:any=document.querySelector("#btnAmpliar");

            contenedor.style.width="1000px";
            contenedor.style.height="500px";
            contenedor.style.margin="5vh auto";

            ampliarLienzo.style.width="1000px";
            ampliarLienzo.style.height="500px";


            btnAmpliar.style.position="relative";
            btnAmpliar.style.top="0";
            btnAmpliar.style.left="0";
            btnAmpliar.style.zIndex="0";

            this.ampliarPa="Ampliar";
            }

        
        }
       canvasCreacion(){
        var canvas:any=document.querySelector("#lienzo");
        var ctx:any=canvas.getContext("2d");
        var frame:any = window.requestAnimationFrame;
        //var temporizador:any=document.querySelector('#temporizador span')
        //POO
        // var objeto:any=new Object({
        //     color:"red",
        //     x:280,
        //     y:70,
        //     ancho:10,
        //     alto:10,
        //     metodo:function(){
        //         objeto.x=10;
        //     }
        // });
        // objeto.metodo(); 
        
var posCanvas = canvas.getBoundingClientRect();
var temporizador = 0;
var mouse = {
    x: 0,
    y: 0
};
var radio = 27;

var i = 0;
var control = 0;
var escribe = false;
var finDeEscritura = false;
var propTexto = {
    tamano: 18,
    interlineado: 10,
    x: 832,
    y: 300
};

var divisor={
    x:307,
    y:103,
    dx:3,
    dy:332
}

var titulos= [
    {
        texto: "INGENIERÍA INDUSTRIAL", //CARRERA
        tamano: 36
    },{
        texto:"HABILIDADES QUE",
        tamano:20
    },{
        texto:"DESARROLLAREMOS",
        tamano:20
    },{
        texto:"DESAFIOS",
        tamano:20
    }
];

var habilidades = [
    {
        nombre: "ANÁLISIS DE DATOS",
        archivo : "analisisdedatos",
        colorBorde:"rgb(153,22,28)",
        colorRelleno:"rgb(197,101,86)",
        avance:10, //VALOR EN PORCENTAJE
        x:87,
        y:166,
        dx:149,
        dy:18
    },{
        nombre: "PLANIFICACIÓN Y ORGANIZACIÓN",
        archivo : "planificacionyorganizacion",
        colorBorde:"rgb(38,123,200)",
        colorRelleno:"rgb(77,169,204)",
        avance:40, //VALOR EN PORCENTAJE
        x:87,
        y:230,
        dx:149,
        dy:18
    },{
        nombre: "GESTIÓN Y ADMINISTRACIÓN",
        archivo : "gestionyadministracion",
        colorBorde:"rgb(78,157,64)",
        colorRelleno:"rgb(168,192,109)",
        avance:80, //VALOR EN PORCENTAJE
        x:87,
        y:294,
        dx:149,
        dy:18
    },{
        nombre: "MANEJO DE CIENCIA Y TECNOLOGÍA",
        archivo : "manejodecienciaytecnologia",
        colorRelleno:"rgb(165,114,168)",
        colorBorde:"rgb(114,52,135)",
        avance:50, //VALOR EN PORCENTAJE
        x:87,
        y:358,
        dx:149,
        dy:18
    },{
        nombre: "DISEÑO Y FABRICACIÓN",
        archivo : "diseñoyfabricacion",
        colorRelleno:"rgb(54,140,178)",
        colorBorde:"rgb(13,108,132)",
        avance:60, //VALOR EN PORCENTAJE
        x:87,
        y:422,
        dx:149,
        dy:18
    }
];

var desafios = [
    {
        nombre: "basica1", //RADIO 27
        x:364,
        y:341,
        color:"rgb(224,184,47)",
        colorSeleccion:"rgb(81,157,90)"
    },{
        nombre: "basica2",
        x:395,
        y:287,
        color:"rgb(224,184,47)",
        colorSeleccion:"rgb(81,157,90)"
    },{
        nombre: "basica3",
        x:453,
        y:293,
        color:"rgb(224,184,47)",
        colorSeleccion:"rgb(81,157,90)"
    },{
        nombre: "basica4",
        x:516,
        y:294,
        color:"rgb(224,184,47)",
        colorSeleccion:"rgb(81,157,90)"
    },{
        nombre: "finanzas1",
        x:549,
        y:237,
        color:"rgb(180,69,61)",
        colorSeleccion:"rgb(92,172,219)"
    },{
        nombre: "finanzas2",
        x:583,
        y:182,
        color:"rgb(180,69,61)",
        colorSeleccion:"rgb(92,172,219)"
    },{
        nombre: "finanzas3",
        x:644,
        y:179,
        color:"rgb(180,69,61)",
        colorSeleccion:"rgb(92,172,219)"
    },{
        nombre: "produccion1",
        x:579,
        y:297,
        colorSeleccion:"rgb(180,69,61)",
        color:"rgb(92,172,219)"
    },{
        nombre: "produccion2",
        x:637,
        y:297,
        colorSeleccion:"rgb(180,69,61)",
        color:"rgb(92,172,219)"
    },{
        nombre: "produccion3",
        x:692,
        y:300,
        colorSeleccion:"rgb(180,69,61)",
        color:"rgb(92,172,219)"
    },{
        nombre: "logistica1",
        x:549,
        y:352,
        colorSeleccion:"rgb(224,184,47)",
        color:"rgb(81,157,90)"
    },{
        nombre: "logistica2",
        x:580,
        y:404,
        colorSeleccion:"rgb(224,184,47)",
        color:"rgb(81,157,90)"
    },{
        nombre: "logistica3",
        x:612,
        y:451,
        colorSeleccion:"rgb(224,184,47)",
        color:"rgb(81,157,90)"
    },
    {
        nombre: "logistica4",
        x:670,
        y:454,
        colorSeleccion:"rgb(224,184,47)",
        color:"rgb(81,157,90)"
    }
];

var texto0 = [
    {
        contenido: "retobasico1",
        mostrado : " "
    },{
        contenido: "orem ipsum dolor sit",
        mostrado : ""
    },{
        contenido: "orem ipsum dolor sit",
        mostrado : ""
    },{
        contenido: "orem ipsum dolor sit",
        mostrado : ""
    },{
        contenido: "orem ipsum dolor sit",
        mostrado : ""
    },
];

var juego = {
    tiempo: function () {
        temporizador++;
        juego.canvas();
        frame(juego.tiempo);
    },
    canvas: function () {

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvas.onmousedown = function (e) {
            mouse.x = e.clientX - posCanvas.left;
            mouse.y = e.clientY - posCanvas.top;
            /*            alert(mouse.x);
                        alert(mouse.y);*/
        };

        pantallas.primera();
        //PRINCIPAL INVOCADOR DE FUNCIONES MEDIANTE MOUSE

        for(var q=0;q<desafios.length;q++){
            if(Math.pow(Math.pow(mouse.x - desafios[q].x,2)+Math.pow(mouse.y - desafios[q].y,2),1/2)<radio){
                console.log("SELECCIONADA " + desafios[q].nombre);
                funciones.contenidoCuadroTexto(texto0,0,0);
                
               // mouse.x=0;
               // mouse.y=0;
            }
        }

    }
};


var funciones = {

    mostrarTitulos:function(){
        
        for (var q=0; q<titulos.length ; q++){
            ctx.beginPath();
            ctx.font = "bold "+titulos[q].tamano+"px Fjalla One";
            ctx.fillStyle = "black";
            var cadena = titulos[q].texto;
            switch(q){
                case 0 : ctx.fillText(cadena, (canvas.width/2) - ctx.measureText(cadena).width/2,50); break;
                case 1 : ctx.fillText(cadena, (divisor.x/2) - ctx.measureText(cadena).width/2,100); break;
                case 2 : ctx.fillText(cadena, (divisor.x/2) - ctx.measureText(cadena).width/2,100+titulos[q].tamano+10); break;
                case 3 : ctx.fillText(cadena, (1000-divisor.x-divisor.dx)/2 +divisor.x - ctx.measureText(cadena).width/2,100); break;
            }
            
            ctx.closePath();
        }
    },
    lineaDivisoria:function(){
        ctx.beginPath();
        ctx.lineJoin = "round";
        ctx.lineWidth = divisor.dx;
        ctx.moveTo(divisor.x, divisor.y);
        ctx.lineTo(divisor.x, divisor.y + divisor.dy);
        ctx.strokeStyle='blue';
        ctx.stroke();
        ctx.closePath();
    },
    mostrarHabilidades:function(){

        for(var q=0; q<habilidades.length ; q++){

            /* MOSTRANDO EL FONDO DE LOS INDICADORES */
            ctx.beginPath();
            ctx.rect(habilidades[q].x,habilidades[q].y,habilidades[q].dx,habilidades[q].dy);
            ctx.arc(habilidades[q].x+habilidades[q].dx,habilidades[q].y+habilidades[q].dy/2,habilidades[q].dy/2,0,2*Math.PI);
            ctx.strokeStyle = habilidades[q].colorBorde;
            ctx.stroke();
            ctx.fillStyle = "white";
            ctx.fill();
            ctx.closePath();

            /* MOSTRANDO EL AVANCE EN LOS INDICADORES */
            ctx.beginPath();
            ctx.rect(habilidades[q].x,habilidades[q].y,habilidades[q].dx*habilidades[q].avance/100,habilidades[q].dy);
            ctx.arc(habilidades[q].x+habilidades[q].dx*habilidades[q].avance/100,habilidades[q].y+habilidades[q].dy/2,habilidades[q].dy/2,0,2*Math.PI);
            ctx.fillStyle = habilidades[q].colorRelleno;
            ctx.fill();
            ctx.closePath();

            /* MOSTRANDO FONDO DEL LOGO DEL INDICADOR */
            ctx.beginPath();
            ctx.lineWidth = 6;
            ctx.arc(habilidades[q].x,habilidades[q].y,habilidades[q].dy,0,2*Math.PI);
            ctx.strokeStyle = habilidades[q].colorBorde;
            ctx.stroke();
            ctx.fillStyle = "white";
            ctx.lineWidth = 3;
            ctx.fill();
            
            ctx.closePath();

            /* MOSTRANDO EL LOGO DESNTRO DE LOS INDICADORES */
            var images = new Image();
            images.src = '../../assets/images/ingsis/'+habilidades[q].archivo+'.png';
            ctx.drawImage(images, habilidades[q].x-20, habilidades[q].y-20, 40,40);

        }

    },
    sombra:function(){
        ctx.beginPath(); //iniciar ruta
        ctx.shadowOffsetX=-3; //desplazamiento horizontal sombra.
        ctx.shadowOffsetY=-2; //desplazamiento vertical sombra
        ctx.shadowColor="rgb(82,92,96)"; //color de sombra
        ctx.shadowBlur=10; //dispersion de sombra.
        ctx.closePath();
    },
    finSombra:function(){
        ctx.beginPath(); //iniciar ruta
        ctx.shadowOffsetX=0; //desplazamiento horizontal sombra.
        ctx.shadowOffsetY=0; //desplazamiento vertical sombra
        ctx.shadowColor="rgba(82,92,96,0)"; //color de sombra
        ctx.shadowBlur=0; //dispersion de sombra.
        ctx.closePath();
    },
    mostrarDesafios:function(){

        

        for(var q=0 ; q<desafios.length ; q++){
            ctx.beginPath();

            ctx.arc(desafios[q].x,desafios[q].y,radio,0,2*Math.PI);
            ctx.fillStyle = desafios[q].color;
            ctx.fill();
            ctx.closePath();

            var images = new Image();
            images.src = '../../assets/images/ingsis/'+desafios[q].nombre+'.png';
            ctx.drawImage(images, desafios[q].x-27.5, desafios[q].y-27.5, 55,55);

        }

    },
    validarSeleccion:function(){

        for( var q=0; q<desafios.length; q++){
            
        }

    },
    contenidoCuadroTexto: function (texto, x, y) {

        ctx.beginPath();
        for (var q = 0; q < texto.length; q++) {
            ctx.font = "bold " + propTexto.tamano + "px Fjalla One";
            ctx.fillStyle = "black";
            ctx.fill();
            if (q == 0) {
                ctx.fillText(texto[q].mostrado, propTexto.x + x, propTexto.y + y);
            } else {

                ctx.fillText(texto[q].mostrado, propTexto.x + x, propTexto.y + y + q * (propTexto.tamano + propTexto.interlineado));
            }
        }

        if (i < texto.length) {

            if (temporizador % 2 == 0) {


                if (control < texto[i].contenido.length) {
                    texto[i].mostrado = texto[i].mostrado + texto[i].contenido[control];
                    control++;
                } else {
                    i++;
                    control = 0;
                }
            }
        } else {
            finDeEscritura = true;

        }
        /*if(finDeEscritura && temporizador%10==0){

            escribe = false;
            }*/
    }
    
};

var pantallas = {
    primera: function () {

        document.getElementById("lienzo").style.background="rgb(204,255,255)";
        ctx.clearRect(0,0,canvas.width,canvas.height);

        /* QUEDA? */
        funciones.sombra();
        funciones.mostrarDesafios();
        funciones.finSombra();
        /* QUEDA? */
        
        

        
        /*var buho = new Image();
        buho.src = 'images/prueba1.png';
        ctx.drawImage(buho, 0, 0, 1000,500);*/
        

        
        //CONTENIDO DE LA PRIMERA PANTALLA
        funciones.mostrarTitulos();
        funciones.lineaDivisoria();
        funciones.mostrarHabilidades();

        var buho = new Image();
        buho.src= "../../assets/images/ingsis/buho2.png"
        ctx.drawImage(buho,784,102,160,110);

        



        

        



    }
};

juego.tiempo();

       }

    //DEGRADADOS

}