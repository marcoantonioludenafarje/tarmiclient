import{Component , OnInit} from '@angular/core';
import{UserService} from '../services/user.service';
import{PreguntaService} from '../services/pregunta.service';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {User} from '../models/user';
import {GLOBAL} from '../services/global';
import {Artist} from '../models/artist';
import{PreguntaTestGeneral,Alternativas,SkillBasico} from '../models/preguntaTestGeneral';
import Chart from 'chart.js';

//naturales
import { Product } from '../models/model';
@Component({
    selector:'editar-test',
    templateUrl:'../views/juegosCanvas.html',
    providers:[UserService,PreguntaService]
})
export class JuegosCanvasComponent implements OnInit{
        public titulo:string;
        public artist:Artist;
        public pregunta:PreguntaTestGeneral;
        public preguntaPrueb:PreguntaTestGeneral;
        public identity;
        public token;
        public url:string;
        public ampliarPa:String="Ampliar";

        constructor(
            private _route:ActivatedRoute,
            private _router:Router,
            private _userService:UserService,
            private _preguntaService:PreguntaService
        ){
            this.titulo='Juegos listo';
        }
        ngOnInit(){

            var auxiliar=document.getElementById("cuerpoCentral");
            var barraHorizontal=document.getElementById("navigation-vertical");
            auxiliar.style.margin="0px";
            auxiliar.style.padding="0px";
            auxiliar.style.width="100vw";
            barraHorizontal.style.display="none";
            document.getElementById("rechargePart").className=document.getElementById("rechargePart").className+ " cuerpoExteriorJuegos";
            this.canvasCreacion();
        }

        ampliar(){
            if(this.ampliarPa=="Ampliar"){
            // var auxiliar=document.getElementById("cuerpoCentral");
            // var barraHorizontal=document.getElementById("navigation-vertical");
            var contenedor:any=document.querySelector("#contenedor-juegos");
            var ampliarLienzo:any=document.querySelector("#lienzo");
            var btnAmpliar:any=document.querySelector("#btnAmpliar");


            contenedor.style.width="100%";
            contenedor.style.height="100vh";
            contenedor.style.margin="0";

            ampliarLienzo.style.width="100%";
            ampliarLienzo.style.height="100vh";
            ampliarLienzo.style.backgroundSize="cover";
            ampliarLienzo.style.backgroundRepeat="no-repeat";

            btnAmpliar.style.position="fixed";
            btnAmpliar.style.top="10px";
            btnAmpliar.style.left="10px";
            btnAmpliar.style.zIndex="1";

            // auxiliar.style.margin="0px";
            // auxiliar.style.padding="0px";
            // auxiliar.style.width="100vw";

            // barraHorizontal.style.display="none";

            this.ampliarPa="Reducir";
            }else{
            var auxiliar=document.getElementById("cuerpoCentral");
            var barraHorizontal=document.getElementById("navigation-vertical");
            var contenedor:any=document.querySelector("#contenedor-juegos");
            var ampliarLienzo:any=document.querySelector("#lienzo");
            var btnAmpliar:any=document.querySelector("#btnAmpliar");

            contenedor.style.width="1000px";
            contenedor.style.height="500px";
            contenedor.style.margin="5vh auto";

            ampliarLienzo.style.width="1000px";
            ampliarLienzo.style.height="500px";


            btnAmpliar.style.position="relative";
            btnAmpliar.style.top="0";
            btnAmpliar.style.left="0";
            btnAmpliar.style.zIndex="0";

            this.ampliarPa="Ampliar";
            }

        
        }
       canvasCreacion(){
        var canvas:any=document.querySelector("#lienzo");
        var ctx:any=canvas.getContext("2d");
        var frame:any = window.requestAnimationFrame;
        var temporizador:any=document.querySelector('#temporizador span')
        //POO
        // var objeto:any=new Object({
        //     color:"red",
        //     x:280,
        //     y:70,
        //     ancho:10,
        //     alto:10,
        //     metodo:function(){
        //         objeto.x=10;
        //     }
        // });
        // objeto.metodo(); 
        var jugador={
            x:280,
            y:70,
            ancho:10,
            alto:10,
            color:"red",
            movimiento_x: 0,
            movimiento_y: 0,
            velocidad:5,
            x1:null,
            x2:null,
            y1:null,
            y2:null
        }
        var bloques:any=[{x:300,y:50,ancho:400,alto:10,x1:null, x2:null, y1:null, y2:null,color:"black"},
               {x:300, y:90, ancho: 10, alto: 360,x1:null, x2:null, y1:null, y2:null},
               {x:300, y:440, ancho: 400, alto: 10,x1:null, x2:null, y1:null, y2:null},
               {x:690, y:90, ancho: 10, alto: 360,x1:null, x2:null, y1:null, y2:null},
               {x:365, y:50, ancho:10, alto:350,x1:null, x2:null, y1:null, y2:null},
			   {x:430, y:100, ancho:10, alto:350,x1:null, x2:null, y1:null, y2:null},
			   {x:495, y:50, ancho:10, alto:350,x1:null, x2:null, y1:null, y2:null},
			   {x:560, y:100, ancho:10, alto:350,x1:null, x2:null, y1:null, y2:null},
               {x:625, y:50, ancho:10, alto:350,x1:null, x2:null, y1:null, y2:null}];


        var datos={
        izquierda:false,
        derecha:false,
        arriba:false,
        abajo:false,
        fotograma:0,
        segundos:0
        };
        var juego={
            teclado:function(){
                //Evento teclado
                document.addEventListener("keydown",juego.oprimir);
                document.addEventListener("keyup",juego.soltar);
 
            },
            oprimir:function(tecla){
                // console.log("tecla",tecla.keyCode);
                tecla.preventDefault();
                switch(tecla.keyCode){
                    case 37: datos.izquierda=true;break;
                    case 38: datos.arriba=true; break;
                    case 39: datos.derecha=true; break;
                    case 40: datos.abajo=true; break;
                }
            },
            soltar:function(tecla){
                tecla.preventDefault();
                switch(tecla.keyCode){
                    case 37: datos.izquierda=false;break;
                    case 38: datos.arriba=false; break;
                    case 39: datos.derecha=false; break;
                    case 40: datos.abajo=false; break;
                }
            },
            tiempo:function(){
                jugador.x +=jugador.movimiento_x;
                if(datos.izquierda){
                    jugador.movimiento_x=-jugador.velocidad;
                    jugador.movimiento_y=0
                }
                if(datos.derecha){
                    jugador.movimiento_x=jugador.velocidad;
                    jugador.movimiento_y=0
                }
                if(!datos.izquierda && !datos.derecha){
                    jugador.movimiento_x=0;
                }

                jugador.y +=jugador.movimiento_y;
                if(datos.arriba){
                    jugador.movimiento_y=-jugador.velocidad;
                    jugador.movimiento_x=0
                }
                if(datos.abajo){
                    jugador.movimiento_y=jugador.velocidad;
                    jugador.movimiento_x=0
                }
                if(!datos.arriba && !datos.abajo){
                    jugador.movimiento_y=0;
                }
                //Todo lo que esta dentro de esta funcion ...da 60 fotogramas por segundos.


               	 	/*=============================================
		COLISIONES
		=============================================*/
		for(var i = 0; i < bloques.length; i++){

			jugador.x1 = jugador.x;
			jugador.x2 = jugador.x + jugador.ancho;
			jugador.y1 = jugador.y;
			jugador.y2 = jugador.y + jugador.alto;

			bloques[i].x1 = bloques[i].x;
			bloques[i].x2 = bloques[i].x + bloques[i].ancho;
			bloques[i].y1 = bloques[i].y;
            bloques[i].y2 = bloques[i].y + bloques[i].alto; 
            
            var colisiones=function (){

				//NO COLISIÓN DE IZQ A DER
				if(jugador.x2 < bloques[i].x1){return false}
				//NO COLISIÓN DE DER A IZQ
				if(jugador.x1 > bloques[i].x2){return false}
				//NO COLISIÓN DE ARRIBA HACIA ABAJO
				if(jugador.y2 < bloques[i].y1){return false}
				//NO COLISIÓN DE ABAJO HACIA ARRIBA
				if(jugador.y1 > bloques[i].y2){return false}

				return true;
			}

			colisiones();

			//COLISIÓN DE IZQ A DER
			if(colisiones() && jugador.x2 < bloques[i].x1 + jugador.movimiento_x){

				jugador.x = 280;
				jugador.y = 55;
				jugador.movimiento_x = 0;
				datos.segundos = 0;
				datos.fotograma = 0;
			}

			//COLISIÓN DE DER A IZQ
			if(colisiones() && jugador.x1 - jugador.movimiento_x > bloques[i].x2){
						
				jugador.x = 280;
				jugador.y = 55;
				jugador.movimiento_x = 0;
				datos.segundos = 0;
				datos.fotograma = 0;
			}

			//COLISIÓN DE ARRIBA HACIA ABAJO
			if(colisiones() && jugador.y2 < bloques[i].y1 + jugador.movimiento_y){
				
				jugador.x = 280;
				jugador.y = 55;
				jugador.movimiento_y = 0;
				datos.segundos = 0;
				datos.fotograma = 0;
			}

			//COLISIÓN DE ABAJO HACIA ARRIBA
			if(colisiones() && jugador.y1 - jugador.movimiento_y > bloques[i].y2){
						
				jugador.x = 280;
				jugador.y = 55;
				jugador.movimiento_y = 0;
				datos.segundos = 0;
				datos.fotograma = 0;
			}

		}

            //Temporizador

            if(datos.fotograma >= 60){

			datos.fotograma = 0;
			datos.segundos ++;

			temporizador.innerHTML = datos.segundos;
		
		}else{

			datos.fotograma ++;
		}


                
            juego.canvas();
            frame(juego.tiempo)
            },
            canvas: function(){
                //Borramos lienzo
                ctx.clearRect(0,0,canvas.width,canvas.height);
                //Dibujar bloques
                ctx.fillStyle=jugador.color;
                ctx.fillRect(jugador.x,jugador.y,jugador.ancho,jugador.alto);
                //Dibujar bloques
                ctx.fillStyle=bloques[0].color;
                for(var i=0;i<bloques.length;i++){
                    ctx.fillRect(bloques[i].x,bloques[i].y,bloques[i].ancho,bloques[i].alto);
                }

            }
        }


        juego.teclado();
        juego.tiempo();

       }

    //DEGRADADOS

}